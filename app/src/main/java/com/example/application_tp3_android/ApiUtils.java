package com.example.application_tp3_android;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import androidx.annotation.RequiresApi;

public class ApiUtils {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean updateTeam(Team team) throws IOException {


        URL searchTeamUrl = WebServiceUrl.buildSearchTeam(team.getName());
        String res = HttpRqst.request(HttpRqst.Type.GET, searchTeamUrl.toString(), null, null);
        InputStream is = new ByteArrayInputStream(res.getBytes("UTF-8"));
        JSONResponseHandlerTeam jsonTeam = new JSONResponseHandlerTeam(team);//Mise a jour toutes les infos
        jsonTeam.readJsonStream(is);

        if(team.getIdLeague() == 0)
            return false; //si l'equipe est introuvable dans l'API

        team = jsonTeam.getTeam();


        URL searchLastEventsUrl = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
        res = HttpRqst.request(HttpRqst.Type.GET, searchLastEventsUrl.toString(), null, null);
        is = new ByteArrayInputStream(res.getBytes("UTF-8"));
        JSONResponseHandlerLastEvents jsonLastEvents = new JSONResponseHandlerLastEvents(team);//Dernier Match
        jsonLastEvents.readJsonStream(is);
        team = jsonLastEvents.getTeam();


        URL getRankingUrl = WebServiceUrl.buildGetRanking(team.getIdLeague());
        res = HttpRqst.request(HttpRqst.Type.GET, getRankingUrl.toString(), null, null);
        is = new ByteArrayInputStream(res.getBytes("UTF-8"));
        JSONResponseHandlerTeamRanking jsonRanking = new JSONResponseHandlerTeamRanking(team); //Classement
        jsonRanking.readJsonStream(is);
        team = jsonRanking.getTeam();

        return true;
    }

    public static Bitmap downloadTeamBadge(String url) {
        HttpURLConnection con = null;
        try {
            URL imageUrl = new URL(url);
            con = (HttpURLConnection) imageUrl.openConnection();
            if(con.getResponseCode() == 200) {
                InputStream is = new BufferedInputStream(con.getInputStream());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                Bitmap img = BitmapFactory.decodeStream(is, null, options);
                return img;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(con != null)
                con.disconnect();
        }
        return null;    //en cas d'erreur
    }

    public static class JSONResponseHandlerTeamRanking {

        private final String TAG = JSONResponseHandlerTeamRanking.class.getSimpleName();

        private Team team;


        public JSONResponseHandlerTeamRanking(Team team) {
            this.team = team;
        }

        public Team getTeam() { return this.team; }

        /**
         * @param response done by the Web service
         *
         */
        public void readJsonStream(InputStream response) throws IOException {

            JsonReader reader = null;
            try {
                reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

                readTable(reader);

            } catch (Exception e){
                e.printStackTrace();
            }finally {
                reader.close();
            }
        }

        public void readTable(JsonReader reader) throws IOException {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("table")) {
                    readArrayTable(reader);
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        }


        private void readArrayTable(JsonReader reader) throws IOException {
            boolean foundTeam = false;
            boolean gotTeamRanking = false;
            reader.beginArray();
            int position = 1; // ranking position
            while (reader.hasNext() ) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (!foundTeam) {
                        if (name.equals("name")) {
                            String s = reader.nextString();
                            if(s.equals(this.team.getName())) {
                                this.team.setRanking(position);
                                gotTeamRanking = true;
                            }
                        } else if (name.equals("total") && gotTeamRanking){
                            this.team.setTotalPoints(reader.nextInt());
                            foundTeam = true;
                        } else {
                            reader.skipValue();
                        }
                    }  else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
                position++;
            }
            reader.endArray();
        }

    }

    public static class JSONResponseHandlerLastEvents {
        private final String TAG = JSONResponseHandlerLastEvents.class.getSimpleName();

        private Team team;
        private Match match;

        public JSONResponseHandlerLastEvents(Team team) {
            this.team = team;
        }

        public Team getTeam() {
            return this.team;
        }

        /**
         * @param response done by the Web service
         */
        public void readJsonStream(InputStream response) throws IOException {
            this.match = new Match();
            JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
            try {
                readResults(reader);
            } finally {
                reader.close();
            }
            this.team.setLastEvent(this.match);
        }

        public void readResults(JsonReader reader) throws IOException {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("results")) {
                    readArrayResults(reader);
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        }


        private void readArrayResults(JsonReader reader) throws IOException {
            boolean foundEventWithNonNullScores = false;
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (!foundEventWithNonNullScores) {
                        if (name.equals("idEvent")) {
                            match.setId(reader.nextLong());
                        } else if (name.equals("strEvent")) {
                            match.setLabel(reader.nextString());
                        } else if (name.equals("strHomeTeam")) {
                            match.setHomeTeam(reader.nextString());
                        } else if (name.equals("strAwayTeam")) {
                            match.setAwayTeam(reader.nextString());
                        } else if (name.equals("intHomeScore") && reader.peek() != JsonToken.NULL) {
                            match.setHomeScore(reader.nextInt());
                        } else if (name.equals("intAwayScore") && reader.peek() != JsonToken.NULL) {
                            match.setAwayScore(reader.nextInt());
                            foundEventWithNonNullScores = true;
                        } else {
                            reader.skipValue();
                        }
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
            }
            reader.endArray();
            if (!foundEventWithNonNullScores)//si on trouve aucun match 'complet'
                match = new Match(-1, "", "", "", -1, -1);
        }

    }



}

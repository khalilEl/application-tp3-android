package com.example.application_tp3_android;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    static final public int ADD_TEAM = 01;
    static final public int UPDATE_TEAM = 02;

    private SwipeRefreshLayout Actualiser;
    private SportDbHelper databaseTeam ;
    private RecyclerView RecyclerViewTeams;
    private Cursor cursor;

    private RecyclerViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        databaseTeam = new SportDbHelper(this);

        if(databaseTeam.getAllTeams().isEmpty())
            databaseTeam.populate();

        //recuperer les données de la BD
       // cursor = databaseTeam.fetchAllTeams();
        //cursor.moveToFirst();

        this.adapter = new RecyclerViewAdapter(this, this.databaseTeam.getAllTeams());

        RecyclerViewTeams = findViewById(R.id.RecyclerViewTeams);
        RecyclerViewTeams.setAdapter(this.adapter);
        RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(this));
        RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));



        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivityForResult(intent, MainActivity.ADD_TEAM);

            }
        });

        this.Actualiser = (SwipeRefreshLayout) findViewById(R.id.refresh);
        this.Actualiser.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new UpdateTeamsAsyncTask().execute();
                MainActivity.this.adapter.notifyItemRangeChanged(0, MainActivity.this.adapter.teams.size());
            }
        });

        ItemTouchHelper ith = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                int idBdd = (int)((ViewHolder)viewHolder).idBdd;
                int position = viewHolder.getAdapterPosition();
                Log.d("Supp", "deleting " + position + "->" + idBdd);
//                supprimer equipe de la base de données
                MainActivity.this.databaseTeam.deleteTeam(idBdd);
//                supprimer equipe de la liste de l'adaptateur
                MainActivity.this.adapter.teams.remove(position);
//                supprimer image logo equipe (si y'en a une)
                String path = MainActivity.this.getApplicationContext().getExternalFilesDir(null).toString();
                new File(path, idBdd + ".png").delete();

                MainActivity.this.adapter.notifyItemRemoved(position);
            }
        });
        ith.attachToRecyclerView(RecyclerViewTeams);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MainActivity.ADD_TEAM && resultCode == RESULT_OK) {
            if(data.hasExtra(Team.TAG)) {
              //  try{
                    Team team = data.getParcelableExtra(Team.TAG);
                    this.databaseTeam.addTeam(team);
                    team = this.databaseTeam.getTeam(team.getName(), team.getLeague()); //recuperer id genere par la base de donnes
                    this.adapter.teams.add(team);
                    this.adapter.notifyItemInserted(this.adapter.teams.size() - 1);
                    this.adapter.notifyDataSetChanged();
              //  } catch (Exception e) {
                //    e.printStackTrace();
              //  }

            }

        }
        else if(requestCode == MainActivity.UPDATE_TEAM && resultCode == RESULT_OK) {
            if(data.hasExtra(Team.TAG)) {
                Team team = data.getParcelableExtra(Team.TAG);
                List<Team> list = this.adapter.teams;

                int position = 0;
                for(Team t : list)
                {
                    if(t.getId() == team.getId())
                        break;
                    else
                        position++;
                }

                this.databaseTeam.updateTeam(team);
                this.adapter.teams.set(position, team);
                this.adapter.notifyItemChanged(position);
            }
        }

    }

    class UpdateTeamsAsyncTask extends AsyncTask {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Object doInBackground(Object[] objects) {
            // Recuperation des equipes de la base de donnes
            List<Team> teams = MainActivity.this.databaseTeam.getAllTeams();

            // Chaque equipe, on update ses donnes
            for(Team t : teams) {
                try {
                    ApiUtils.updateTeam(t);

                    int position = 0;
                    for(Team e : teams)
                    {
                        if(e.getId() == t.getId())
                            break;
                        else
                            position++;
                    }
                    MainActivity.this.databaseTeam.updateTeam(t);
                    Log.wtf("MainActivity", position+" "+t.getName());

                    MainActivity.this.adapter.teams.set(position, t);

                    //badge
                    String path = MainActivity.this.getApplicationContext().getExternalFilesDir(null).toString();
                    File file = new File(path, t.getId() + ".png");
                    if(!file.exists())
                    {
                        Bitmap img = ApiUtils.downloadTeamBadge(t.getTeamBadge());

                        if(img != null && ContextCompat.checkSelfPermission(MainActivity.this.getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                        {
                            OutputStream os = null;
                            os = new FileOutputStream(file);
                            img.compress(Bitmap.CompressFormat.PNG, 85, os);
                            os.close();

                            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            MainActivity.this.Actualiser.setRefreshing(false);
            //Actualisation des infomations de la liste
            MainActivity.this.adapter.notifyDataSetChanged();
        }
    }


}

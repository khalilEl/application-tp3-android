package com.example.application_tp3_android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.example.application_tp3_android.Team.TAG;

public class RecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {

    private Context context;
    public List<Team> teams;

    public RecyclerViewAdapter(Context context, List<Team> equipes) {
        this.context = context;
        this.teams = equipes;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;

    }

    @Override
    //On lie notre recycler view avec notre animalList pour recuperer le nom de l'equipe
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Team equipe = this.teams.get(position);
        final String nomEquipe = equipe.getName();
        String dernierMatch = equipe.getLastEvent().toString();
        String League = equipe.getLeague();

//        badge
        String dirPath = this.context.getExternalFilesDir(null).toString();
        File imageFile = new File(dirPath, equipe.getId() + ".png");
        if(imageFile.exists()) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap img = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
            holder.imgView_Team.setImageBitmap(img);
        }

        holder.txtView_league.setText(League);
        holder.txtView_nameTeam.setText(nomEquipe);
        holder.txtView_LastMatch.setText(dernierMatch);
        holder.idBdd = equipe.getId();//

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked on : "+nomEquipe);

                Intent intent = new Intent(v.getContext(),TeamActivity.class);
                intent.putExtra(TAG, equipe);
                Log.d("Click", "onClick: " + equipe.getId());
                ((Activity)v.getContext()).startActivityForResult(intent, MainActivity.UPDATE_TEAM);
            }
        });



    }

    @Override
    public int getItemCount() {
        return this.teams.size();
    }


}


class ViewHolder extends RecyclerView.ViewHolder {

    ImageView imgView_Team;
    TextView txtView_league;
    TextView txtView_nameTeam;
    TextView txtView_LastMatch;
    LinearLayout parentLayout;
    long idBdd;


    ViewHolder(View row) {
        super(row);
        txtView_league =  row.findViewById(R.id.txtView_league);
        txtView_nameTeam =  row.findViewById(R.id.txtView_nameTeam);
        txtView_LastMatch =  row.findViewById(R.id.txtView_LastMatch);
        parentLayout = itemView.findViewById(R.id.parentLayout);
        imgView_Team =  row.findViewById(R.id.imgView_Team);
    }
    /*
    @Override
    public void onClick(View v) {

        Log.d(TAG, "onClick: clicked on : "+txtView_nameTeam.getText());

        Intent intent = new Intent(v.getContext(),TeamActivity.class);
        intent.putExtra(TAG, t.getText());
        Log.d("Click", "onClick: " + txtView_nameTeam.getText());
        ((Activity)v.getContext()).startActivityForResult(intent, MainActivity.UPDATE_TEAM);

    }
    */

   // void bindModel(String item) {
        // label.setText(item);
        // Animal animal = AnimalList.getAnimal(item);
        //icon.setImageResource(this.itemView.getContext().getResources().getIdentifier(animal.getImgFile(), "drawable", this.itemView.getContext().getPackageName()));

    //}

}
package com.example.application_tp3_android;

import android.os.Build;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import androidx.annotation.RequiresApi;

public class HttpRqst {


         // Envoie du requete HTTP
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public static String request(Type reqType, String url, String[] headers, String data) throws IOException {
            URL u = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) u.openConnection();

            connection.setRequestMethod(reqType.toString());

            if(headers != null) {
                String field, value;
                for(String s : headers) {
                    if(s != null) {
                        field = s.substring(0, s.indexOf(':'));
                        value = s.substring(s.indexOf(':') + 1);
                        connection.setRequestProperty(field, value);
                    }
                }
            }

            if(data != null) {
                connection.setDoOutput(true);
                OutputStream outputStream = connection.getOutputStream();
                byte[] b = data.getBytes("UTF-8");
                outputStream.write(b);
                outputStream.flush();
                outputStream.close();
            }

            System.out.println("Storing the response..");
            StringBuilder content;

            try (BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                String line;
                content = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }
            finally {
                connection.disconnect();
            }

            return content.toString();
        }


        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public static String exec(Request r) throws IOException {
            return HttpRqst.request(r.reqType, r.url, r.headers, r.data);
        }

        static public enum Type { GET, POST; }

        static public class Request {
            public Type reqType;
            public String url;
            public String[] headers;
            public String data;

            public Request(Type reqType, String url, String[] headers, String data) {
                this.reqType = reqType;
                this.url = url;
                this.headers = headers;
                this.data = data;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public String toString() {
                try {
                    return HttpRqst.exec(this);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return "";	//in case of error
            }
        }

}



